#!/bin/bash

set -e

curl https://sh.rustup.rs -sSf | sh
cargo install just