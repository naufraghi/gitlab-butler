use anyhow::{bail, Result};
use log::debug;
use slug::slugify;
use structopt::StructOpt;

use crate::git;
use crate::milestone::print_milestones;
use crate::pipeline::print_pipelines;
use gitlab_butler_lib::client::Client;
use gitlab_butler_lib::entities::MergeRequestState;
use gitlab_butler_lib::entities::{CreateMergeRequest, MergeRequest, ProjectMilestone};
use gitlab_butler_lib::merge_requests::*;

pub fn print_mr(mr: &MergeRequest) {
    bunt::println!(
        "{$italic}{$yellow}MR{/$}{/$}: {$bold}{}{/$} {$cyan}{}{/$}",
        mr.title,
        mr.references.full,
    );
    bunt::println!("{$italic}{$yellow}Url{/$}{/$}: {}", mr.web_url,);
    bunt::println!(
        "{$italic}{$yellow}Branches{/$}{/$}: {} -> {}",
        mr.source_branch,
        mr.target_branch
    );
    if let Some(ref issue) = mr.main_issue {
        bunt::println!(
            "{$italic}{$yellow}Issue{/$}{/$}: {} {$green}{}{/$}",
            issue.title,
            issue.references.full,
        );
    }
    if let Some(ref description) = mr.description {
        bunt::println!(
            "{$italic}{$yellow}Description{/$}{/$}:\n{}",
            description
                .lines()
                .map(|line| ["  ", line].concat())
                .collect::<Vec<String>>()
                .join("\n")
        );
    }
    println!();
}

#[derive(StructOpt, Debug)]
#[structopt(rename_all = "kebab-case")]
pub struct MergeSubcommand {
    #[structopt(subcommand)]
    pub cmd: MergeRequests,
}

pub fn command(client: &Client, mr_command: MergeRequests) -> Result<()> {
    fn execute(shell_command: &str, mr: &MergeRequest) {
        use std::process::Command;
        let mut mr_command = Command::new("bash");
        mr_command
            .arg("-c")
            .arg(shell_command)
            .env("project_id", &mr.project_id.to_string())
            .env("mr_id", &mr.iid.to_string())
            .env("mr_reference", &mr.references.short)
            .env("mr_full_reference", &mr.references.full)
            .env("mr_title", &mr.title)
            .env("mr_source", &mr.source_branch)
            .env("mr_target", &mr.target_branch)
            .env("mr_web_url", &mr.web_url);
        if let Some(issue) = &mr.main_issue {
            mr_command
                .env("issue_id", &issue.iid.to_string())
                .env("issue_reference", &issue.references.short)
                .env("issue_full_reference", &issue.references.full)
                .env("issue_title", &issue.title)
                .env(
                    "issue_slug",
                    slugify(format!("{}-{}", &issue.iid, &issue.title)),
                )
                .env("issue_web_url", &issue.web_url);
            if let Some(description) = &issue.description {
                mr_command.env("issue_description", description);
            }
        }
        if let Some(description) = &mr.description {
            mr_command.env("mr_description", description);
        }
        mr_command
            .spawn()
            .expect(&format!("Error executing: {}", shell_command));
    }
    match mr_command {
        MergeRequests::NewMergeRequest(new_mr) => {
            let milestone = match new_mr.milestone.as_ref() {
                None => None,
                Some(milestone_title) => {
                    let milestone = ProjectMilestone::in_project_by_title(
                        client,
                        &new_mr.project,
                        milestone_title,
                    )?;
                    if milestone.is_none() {
                        let milestones = ProjectMilestone::in_project(client, &new_mr.project)?;
                        print_milestones(milestones);
                        bail!("Ambiguous milestone");
                    }
                    milestone
                }
            };
            let mut create_mr: CreateMergeRequest = new_mr.into();
            create_mr.milestone_id = milestone.map(|m| m.id);
            debug!("Creating new_mr: {:?}", create_mr);
            print_mr(&MergeRequest::create(client, &create_mr)?);
            Ok(())
        }
        MergeRequests::MergeReady(merges) => {
            mergebot(client, &merges.project, &merges.labels, &merges.emoji)
        }
        MergeRequests::ListMergeRequests(mrs) => {
            let mr_list = MergeRequest::list(
                client,
                &mrs.project,
                &mrs.state,
                &mrs.labels,
                &mrs.milestone,
                &None,
                mrs.limit,
            )?;
            let mr_list = mr_list.iter().filter(|mr| {
                if mrs.only_with_issue {
                    mr.main_issue.is_some()
                } else {
                    true
                }
            });
            if let Some(ref shell_command) = mrs.execute {
                for mr in mr_list {
                    execute(shell_command, mr);
                }
            } else {
                for mr in mr_list {
                    println!();
                    print_mr(mr);
                    if !mrs.no_pipelines {
                        print_pipelines(mr.pipelines(client)?);
                    }
                    println!();
                }
            }
            Ok(())
        }
        MergeRequests::SingleMergeRequest(single_mr) => {
            let mr = if let Some(mr_iid) = single_mr.mr_iid {
                MergeRequest::single(client, &single_mr.project, mr_iid)?
            } else {
                let branch = git::get_branch_name();
                let mr = branch.and_then(|name| {
                    MergeRequest::list(
                        client,
                        &None,
                        &MergeRequestState::Opened,
                        &None,
                        &None,
                        &Some(name),
                        1,
                    )
                    .ok()?
                    .into_iter()
                    .next()
                });
                if mr.is_none() {
                    bail!("Unable to find merge request from branch")
                } else {
                    mr.unwrap()
                }
            };
            if let Some(ref shell_command) = single_mr.execute {
                execute(shell_command, &mr);
            } else {
                print_mr(&mr);
                if !single_mr.no_pipelines {
                    print_pipelines(mr.pipelines(client)?);
                }
            }
            Ok(())
        }
        MergeRequests::MergeRequestEdit(mr_edit) => {
            let mr = if let Some(mr_iid) = mr_edit.mr_iid {
                MergeRequest::single(client, &mr_edit.project, mr_iid)?
            } else {
                let branch = git::get_branch_name();
                let mr = branch.and_then(|name| {
                    MergeRequest::list(
                        client,
                        &None,
                        &MergeRequestState::Opened,
                        &None,
                        &None,
                        &Some(name),
                        1,
                    )
                    .ok()?
                    .into_iter()
                    .next()
                });
                if mr.is_none() {
                    bail!("Unable to find merge request from branch")
                } else {
                    mr.unwrap()
                }
            };
            if let Some(ref shell_command) = mr_edit.execute {
                execute(shell_command, &mr);
            } else {
                print_mr(&mr);
            }
            Ok(())
        }
    }
}

#[derive(StructOpt, Debug)]
#[structopt(rename_all = "kebab-case")]
pub enum MergeRequests {
    #[structopt(name = "new")]
    /// Create a new merge request
    NewMergeRequest(NewMergeRequest),
    // -------------------------------------------------------------------------
    #[structopt(name = "get")]
    /// Single merge request (defaults to auto detect from current branch)
    SingleMergeRequest(SingleMergeRequest),
    // -------------------------------------------------------------------------
    #[structopt(name = "list")]
    /// List merge requests
    ListMergeRequests(ListMergeRequests),
    // -------------------------------------------------------------------------
    #[structopt(name = "edit")]
    /// Edit merge request
    MergeRequestEdit(MergeRequestEdit),
    // -------------------------------------------------------------------------
    /// Rebase and merge ready MRs
    MergeReady(MergeReady),
}

#[derive(StructOpt, Debug)]
#[structopt(rename_all = "kebab-case")]
pub struct SingleMergeRequest {
    #[structopt(long, short = "p", env = "GITLAB_BUTLER_PROJECT")]
    /// Project name or id (defaults to CI_PROJECT_PATH)
    pub project: String,
    #[structopt(name = "id", env = "mr_id")]
    /// Merge request id
    pub mr_iid: Option<usize>,
    #[structopt(long, short = "x")]
    /// Execute command for each returned mr (in a bash subshell)
    ///
    /// See `mr list` help for more information.
    pub execute: Option<String>,
    // ------------------------------
    #[structopt(long)]
    /// Print latest pipelines status (CI jobs)
    pub no_pipelines: bool,
}

#[derive(StructOpt, Debug)]
#[structopt(rename_all = "kebab-case")]
pub struct NewMergeRequest {
    #[structopt(long, short = "p", env = "GITLAB_BUTLER_PROJECT")]
    /// Project name or id (defaults to CI_PROJECT_PATH)
    pub project: String,
    // -------------------------------------------------------------------------
    /// Merge request title
    pub title: String,
    // -------------------------------------------------------------------------
    /// Merge request description
    #[structopt(long, short = "d")]
    pub description: Option<String>,
    // -------------------------------------------------------------------------
    #[structopt(long, short = "s", env = "GITLAB_BUTLER_SOURCE_BRANCH")]
    /// Source branch
    pub source_branch: String,
    // -------------------------------------------------------------------------
    #[structopt(
        long,
        short = "t",
        env = "GITLAB_BUTLER_TARGET_BRANCH",
        default_value = "master"
    )]
    /// Target branch
    pub target_branch: String,
    // -------------------------------------------------------------------------
    #[structopt(long, short = "l")]
    /// Comma-separated list of label names
    pub labels: Option<String>,
    // -------------------------------------------------------------------------
    #[structopt(long, short = "m")]
    /// Milestone name
    pub milestone: Option<String>,
}

#[derive(StructOpt, Debug)]
#[structopt(rename_all = "kebab-case")]
pub struct ListMergeRequests {
    #[structopt(long, short = "s", default_value = "opened")]
    /// Filter merge requests by state: opened, closed, merged
    pub state: MergeRequestState,
    // -------------------------------------------------------------------------
    #[structopt(long, short = "p", env = "GITLAB_BUTLER_PROJECT")]
    /// Project name or id (defaults to CI_PROJECT_PATH)
    pub project: Option<String>,
    // -------------------------------------------------------------------------
    #[structopt(long, short = "l", verbatim_doc_comment)]
    /// Filter merge requests by comma-separated list of label names
    ///
    /// Merge requests must have all labels to be returned.
    /// - `None` lists all merge requests with no labels. `Any` lists all merge requests with at least one label.
    /// - `No+Label` (Deprecated) lists all merge requests with no labels. Predefined names are case-insensitive.
    pub labels: Option<String>,
    // -------------------------------------------------------------------------
    #[structopt(long, short = "m")]
    /// Filter merge requests by the milestone title. `None` lists all merge requests with no milestone.
    ///
    /// - `Any` lists all merge requests that have an assigned milestone.
    pub milestone: Option<String>,
    // -------------------------------------------------------------------------
    #[structopt(long, short = "x", verbatim_doc_comment)]
    /// Execute command for each returned merge request (in a bash subshell)
    ///
    /// The current merge request is exported as environment variables:
    ///   - `mr_id`: 123
    ///   - `mr_title`: Resolve some problem
    ///   - `mr_source`: 1234-some-problem
    ///   - `mr_target`: master
    ///   - `mr_description` (optional): Long description
    ///   - `mr_reference`: !123
    ///   - `mr_full_reference`: group/project!123
    ///   - `mr_web_url`: http://gitlab.com/group/project/merge_requests/123
    ///
    /// If the MR has a "main_issue", issue variables are exported too.
    /// A "main_issue" is the one embedded in the branch name.
    ///
    /// Example:
    ///   - `... mr list -x 'echo ${mr_reference}: ${mr_title}'`
    ///
    /// Or to use the fully qualified reference:
    ///   - `... mr list -x 'echo ${mr_full_reference}: ${mr_title}'`
    pub execute: Option<String>,
    // -------------------------------------------------------------------------
    #[structopt(long, short = "n", default_value = "100")]
    pub limit: usize,
    // ------------------------------
    #[structopt(long, short = "i")]
    /// List only MRs with a main issue (from branch name)
    pub only_with_issue: bool,
    // ------------------------------
    #[structopt(long)]
    /// Print latest pipelines status (CI jobs)
    pub no_pipelines: bool,
}

#[derive(StructOpt, Debug, Clone)]
#[structopt(rename_all = "kebab-case")]
pub struct MergeRequestEdit {
    #[structopt(long, short = "p", env = "GITLAB_BUTLER_PROJECT")]
    /// Project name or id (defaults to CI_PROJECT_PATH)
    pub project: String,
    // -------------------------------------------------------------------------
    #[structopt(name = "id", env = "mr_id")]
    /// Merge request id (default: autodetect from current branch)
    pub mr_iid: Option<usize>,
    // -------------------------------------------------------------------------
    /// Edited merge request title
    #[structopt(long, short = "t")]
    pub title: Option<String>,
    // -------------------------------------------------------------------------
    /// Edited merge request description
    #[structopt(long, short = "d")]
    pub description: Option<String>,
    // -------------------------------------------------------------------------
    #[structopt(long, short = "l")]
    /// Comma-separated list of label names (empty to remove all)
    pub labels: Option<String>,
    // -------------------------------------------------------------------------
    #[structopt(long, short = "x")]
    /// Execute command for each returned merge request (in a bash subshell)
    ///
    /// See `mr list` help for more information.
    pub execute: Option<String>,
}

#[derive(StructOpt, Debug)]
#[structopt(rename_all = "kebab-case")]
pub struct MergeReady {
    #[structopt(long, short = "p", env = "GITLAB_BUTLER_PROJECT")]
    /// Project name or id (defaults to CI_PROJECT_PATH)
    pub project: Option<String>,
    // -------------------------------------------------------------------------
    #[structopt(
        long,
        short = "l",
        env = "GITLAB_BUTLER_MERGE_LABELS",
        default_value = "Merge+ready+🐙"
    )]
    /// Labels to search for ready to merge MRs, comma separated
    pub labels: String,
    // -------------------------------------------------------------------------
    #[structopt(
        long,
        short = "e",
        env = "GITLAB_BUTLER_MERGE_EMOJI",
        default_value = "octopus"
    )]
    /// Emoji to search for ready to merge MRs
    pub emoji: String,
}
