use anyhow::{bail, Result};
use log::debug;
use slug::slugify;
use structopt::StructOpt;

use crate::git;
use crate::milestone::print_milestones;
use gitlab_butler_lib::client::Client;
use gitlab_butler_lib::entities::{CreateIssue, EditIssue, Issue, IssueState, StateEvent, ProjectMilestone};

pub fn print_issue(issue: &Issue) {
    bunt::println!(
        "{$italic}{$yellow}Issue{/$}{/$}: {$bold}{}{/$} {$cyan}{}{/$}",
        issue.title,
        issue.references.full,
    );
    bunt::println!("{$italic}{$yellow}Url{/$}{/$}: {}", issue.web_url,);

    if let Some(ref milestone) = issue.milestone {
        bunt::println!("{$italic}{$yellow}Milestone{/$}{/$}: {}", milestone.title,);
    }
    if let Some(ref description) = issue.description {
        bunt::println!(
            "{$italic}{$yellow}Description{/$}{/$}:\n{}",
            description
                .lines()
                .map(|line| ["  ", line].concat())
                .collect::<Vec<String>>()
                .join("\n")
        );
    }
}

#[derive(StructOpt, Debug)]
#[structopt(rename_all = "kebab-case")]
pub struct IssueSubcommand {
    #[structopt(subcommand)]
    pub cmd: Issues,
}
#[derive(StructOpt, Debug)]
#[structopt(rename_all = "kebab-case")]
pub enum Issues {
    #[structopt(name = "get")]
    /// Single issue (defaults to autodetect from current branch)
    SingleIssue(SingleIssue),
    // -------------------------------------------------------------------------
    #[structopt(name = "list")]
    /// List issues
    ListIssues(ListIssues),
    // -------------------------------------------------------------------------
    #[structopt(name = "new")]
    /// New issue
    NewIssue(NewIssue),
    // -------------------------------------------------------------------------
    #[structopt(name = "edit")]
    /// Edit issue (defaults to autodetect from current branch)
    IssueEdit(IssueEdit),
}

#[derive(StructOpt, Debug)]
#[structopt(rename_all = "kebab-case")]
pub struct SingleIssue {
    #[structopt(long, short = "p", env = "GITLAB_BUTLER_PROJECT")]
    /// Project name or id (defaults to CI_PROJECT_PATH)
    pub project: String,
    // -------------------------------------------------------------------------
    #[structopt(name = "id", env = "issue_id")]
    /// Issue id (default: autodetect from current branch)
    pub issue_iid: Option<usize>,
    // -------------------------------------------------------------------------
    #[structopt(long, short = "x")]
    /// Execute command for each returned issue (in a bash subshell)
    ///
    /// See `issue list` help for more information.
    pub execute: Option<String>,
}

#[derive(StructOpt, Debug)]
#[structopt(rename_all = "kebab-case")]
pub struct ListIssues {
    #[structopt(long, short = "s", default_value = "opened")]
    /// Filter issues by state: all, opened, closed
    pub state: IssueState,
    // -------------------------------------------------------------------------
    #[structopt(long, short = "p", env = "GITLAB_BUTLER_PROJECT")]
    /// Project name or id (defaults to CI_PROJECT_PATH)
    pub project: Option<String>,
    // -------------------------------------------------------------------------
    #[structopt(long, short = "l", verbatim_doc_comment)]
    /// Filter issues by comma-separated list of label names
    ///
    /// Issues must have all labels to be returned.
    /// - `None` lists all issues with no labels. `Any` lists all issues with at least one label.
    /// - `No+Label` (Deprecated) lists all issues with no labels. Predefined names are case-insensitive.
    pub labels: Option<String>,
    // -------------------------------------------------------------------------
    #[structopt(long, short = "m")]
    /// Filter issues by the milestone title. `None` lists all issues with no milestone.
    ///
    /// - `Any` lists all issues that have an assigned milestone.
    pub milestone: Option<String>,
    // -------------------------------------------------------------------------
    #[structopt(long, short = "x", verbatim_doc_comment)]
    /// Execute command for each returned issue (in a bash subshell)
    ///
    /// The current issue is exported as environment variables:
    ///   - `issue_id`: 234
    ///   - `issue_title`: Resolve some problem
    ///   - `issue_description` (optional): Long description
    ///   - `issue_slug`: 234-resolve-some-problem
    ///   - `issue_reference`: #234
    ///   - `issue_full_reference`: group/project#234
    ///   - `issue_web_url`: http://gitlab.com/group/project/issues/234
    ///
    /// Example:
    ///   - `... issue list -x 'echo ${issue_reference}: ${issue_title}'`
    ///
    /// Or to use the fully qualified reference:
    ///   - `... issue list -x 'echo ${issue_full_reference}: ${issue_title}'`
    pub execute: Option<String>,
    // -------------------------------------------------------------------------
    #[structopt(long, short = "n", default_value = "100")]
    pub limit: usize,
}

#[derive(StructOpt, Debug, Clone)]
#[structopt(rename_all = "kebab-case")]
pub struct NewIssue {
    #[structopt(long, short = "p", env = "GITLAB_BUTLER_PROJECT")]
    /// Project name or id (defaults to CI_PROJECT_PATH)
    pub project: String,
    // -------------------------------------------------------------------------
    /// Issue title
    pub title: String,
    // -------------------------------------------------------------------------
    /// Issue description
    #[structopt(long, short = "d")]
    pub description: Option<String>,
    // -------------------------------------------------------------------------
    #[structopt(long, short = "l")]
    /// Comma-separated list of label names
    pub labels: Option<String>,
    // -------------------------------------------------------------------------
    #[structopt(long, short = "m")]
    /// Milestone name
    pub milestone: Option<String>,
    // -------------------------------------------------------------------------
    #[structopt(long, short = "x")]
    /// Execute command for each returned issue (in a bash subshell)
    ///
    /// See `issue list` help for more information.
    pub execute: Option<String>,
}

#[derive(StructOpt, Debug, Clone)]
#[structopt(rename_all = "kebab-case")]
pub struct IssueEdit {
    #[structopt(long, short = "p", env = "GITLAB_BUTLER_PROJECT")]
    /// Project name or id (defaults to CI_PROJECT_PATH)
    pub project: String,
    // -------------------------------------------------------------------------
    #[structopt(name = "id", env = "issue_id")]
    /// Issue id (default: autodetect from current branch)
    pub issue_iid: Option<usize>,
    // -------------------------------------------------------------------------
    /// Edited issue title
    #[structopt(long, short = "t")]
    pub title: Option<String>,
    // -------------------------------------------------------------------------
    /// Edited issue description
    #[structopt(long, short = "d")]
    pub description: Option<String>,
    // -------------------------------------------------------------------------
    #[structopt(long, short = "l")]
    /// Comma-separated list of label names (empty to remove all)
    pub labels: Option<String>,
    // -------------------------------------------------------------------------
    #[structopt(long, short = "s")]
    /// New issue state event: close or reopen
    pub state_event: Option<StateEvent>,
    // -------------------------------------------------------------------------
    #[structopt(long, short = "x")]
    /// Execute command for each returned issue (in a bash subshell)
    ///
    /// See `issue list` help for more information.
    pub execute: Option<String>,
}


pub fn command(client: &Client, issue_cmd: Issues) -> Result<()> {
    fn execute(shell_command: &str, issue: &Issue) {
        use std::process::Command;
        let mut issue_command = Command::new("bash");
        issue_command
            .arg("-c")
            .arg(&shell_command)
            .env("project_id", &issue.project_id.to_string())
            .env("issue_id", &issue.iid.to_string())
            .env("issue_reference", &issue.references.short)
            .env("issue_full_reference", &issue.references.full)
            .env("issue_web_url", &issue.web_url)
            .env(
                "issue_slug",
                slugify(format!("{}-{}", &issue.iid, &issue.title)),
            )
            .env("issue_title", &issue.title);
        if let Some(description) = &issue.description {
            issue_command.env("issue_description", description);
        }
        issue_command
            .spawn()
            .expect(&format!("Error executing: {}", &shell_command));
    }
    fn get_issue_iid_from_current_branch() -> Option<usize> {
        let branch = git::get_branch_name();
        branch.and_then(|name| {
            name.split("-")
                .next()
                .and_then(|prefix| prefix.parse::<usize>().ok())
        })
    }
    match issue_cmd {
        Issues::NewIssue(new_issue) => {
            let milestone = match new_issue.milestone.as_ref() {
                None => None,
                Some(milestone_title) => {
                    let milestone = ProjectMilestone::in_project_by_title(
                        &client,
                        &new_issue.project,
                        milestone_title,
                    )?;
                    if milestone.is_none() {
                        let milestones = ProjectMilestone::in_project(&client, &new_issue.project)?;
                        print_milestones(milestones);
                        bail!("Ambiguous milestone");
                    }
                    milestone
                }
            };
            let mut create_issue: CreateIssue = new_issue.clone().into();
            create_issue.milestone_id = milestone.map(|m| m.id);
            debug!("Creating new_issue: {:?}", create_issue);
            let issue = Issue::create(&client, &create_issue)?;
            if let Some(shell_command) = &new_issue.execute {
                execute(shell_command, &issue);
            } else {
                print_issue(&issue);
            }
            Ok(())
        }
        Issues::SingleIssue(single_issue) => {
            let issue_iid = single_issue
                .issue_iid
                .or_else(get_issue_iid_from_current_branch);
            if issue_iid.is_none() {
                bail!("Unable to detect issue id from current branch");
            }
            let issue = Issue::single(&client, &single_issue.project, issue_iid.unwrap())?;
            if let Some(ref shell_command) = single_issue.execute {
                execute(shell_command, &issue);
            } else {
                print_issue(&issue);
            }
            Ok(())
        }
        Issues::ListIssues(issues) => {
            let issue_list = Issue::list(
                &client,
                &issues.project,
                &issues.state,
                &issues.labels,
                &issues.milestone,
                issues.limit,
            )?;

            if let Some(ref shell_command) = issues.execute {
                for issue in issue_list {
                    execute(shell_command, &issue);
                }
            } else {
                for issue in issue_list {
                    println!();
                    print_issue(&issue);
                    println!();
                }
            }
            Ok(())
        }
        Issues::IssueEdit(issue_edit) => {
            let issue_iid = issue_edit
                .issue_iid
                .or_else(get_issue_iid_from_current_branch);
            if issue_iid.is_none() {
                bail!("Unable to detect issue id from current branch");
            }
            let edit_issue: EditIssue = issue_edit.clone().into();
            if !edit_issue.has_some() {
                bail!("Provide some field to edit");
            }
            let issue = Issue::edit(&client, &issue_edit.project, issue_iid.unwrap(),
                                    &edit_issue)?;
            if let Some(ref shell_command) = issue_edit.execute {
                execute(shell_command, &issue);
            } else {
                print_issue(&issue);
            }
            Ok(())
        }
    }
}
